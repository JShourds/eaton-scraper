from __future__ import print_function

import datetime
import os
import time as t
from datetime import *
from multiprocessing.pool import ThreadPool

import pymysql
import requests
import schedule
from requests.auth import HTTPDigestAuth
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as cond
from selenium.webdriver.support.ui import WebDriverWait

master_dict = {}
ipList = []
user = 'admin'
password = 'admin'
phase_dict = {}
power_dict = {}

# ont1 = pd.read_csv('PDUTracker.csv', dtype=object)
# ont = ont1.replace(numpy.nan, '', regex=True)

chromedriver = "/home/Admin/Desktop/Eaton/chromedriver"

os.environ["webdriver.chrome.driver"] = chromedriver


# Gets power draw for Eaton ePDU's #

class Scraper:
    def __init__(self):
        self.unreachable = 0
        self.failed = 0
        self.success = 0
        self.start_time = t.time()
        self.power_dict = {}
        self.good_list = []

        self.power1 = ''
        self.power2 = ''
        self.power3 = ''
        self.load1 = ''
        self.load2 = ''
        self.load3 = ''

    def getInfo(self, ip):
        url = ip
        # global self.power1, self.load1, self.power2, self.load2, self.power3, self.load3
        # global master_dict
        # global good_list
        # global power_dict

        # This is important for keeping Chrome from actually opening!! #
        options = Options()
        options.add_argument("--no-sandbox")
        options.add_argument("start-maximized")
        options.add_argument("disable-infobars")
        options.add_argument("--disable-extensions")
        options.add_argument("--disable-gpu")
        options.add_argument("--disable-dev-shm-usage")

        prefs = {"profile.managed_default_content_settings.images": 2}
        options.add_experimental_option("prefs", prefs)

        options.add_argument('headless')
        driver = webdriver.Chrome(options=options)
        driver.set_page_load_timeout(5)

        try:
            ip = 'http://' + ip
            s = requests.Session()

            self.r = s.post(ip, auth=HTTPDigestAuth(user, password), timeout=.025)
            # print(self.r.status_code)
            if self.r.status_code == 200:
                self.good_list.append(str(ip))

        except Exception as e:
            pass

        if ip in self.good_list:  # Checking if PDU is online
            try:
                print(ip)
                URL = (str(ip) + '/')

                driver.get(URL)
            except Exception as e:
                pass

            try:
                path = 0
                # Looking to see if login is required
                # If login required to see homepage, it logs in then scrapes data, otherwise it passes down to a
                # function that skips the login process
                WebDriverWait(driver, 3).until(cond.presence_of_element_located((By.ID, 'username')))

                try:
                    driver.find_element_by_id('username').send_keys(user)  # Username field
                    driver.find_element_by_id('password').send_keys(password + Keys.RETURN)
                    try:
                        try:
                            # The path to the power values isn't always the same, so we need to hardcode the possible paths here to check which exists #
                            WebDriverWait(driver, 2).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 1
                        except:
                            pass
                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 2
                        except:
                            pass
                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 3
                        except:
                            pass
                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 4
                        except:
                            pass
                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 5
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 6
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 7
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 8
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 9
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 10
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 11
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 12
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 13
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen11"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 14
                        except:
                            pass

                        try:
                            WebDriverWait(driver, .1).until(
                                cond.presence_of_element_located((
                                    By.XPATH,
                                    '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                            path = 15
                        except:
                            pass

                        self.power1 = ''
                        self.power2 = ''
                        self.power3 = ''
                        self.load1 = ''
                        self.load2 = ''
                        self.load3 = ''

                        if path == 1:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen62"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen62"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen62"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 2:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen71"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen71"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen71"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 3:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen114"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen114"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen114"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 4:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen117"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen117"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen117"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 5:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen3"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen3"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen3"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        elif path == 6:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen84"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen84"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen84"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 7:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen4"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen4"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen4"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 8:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen115"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen115"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen115"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 9:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen60"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen60"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen60"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 10:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen67"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen67"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen67"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 11:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen75"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen75"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen75"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 12:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen36"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen36"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen36"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 13:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen110"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen110"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen110"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 14:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen112"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen112"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen112"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen112"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen112"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen112"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        elif path == 15:
                            self.power1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.power2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.power3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                            self.load1 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen69"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                            self.load2 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen69"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                            self.load3 = driver.find_element_by_xpath(
                                '//*[@id="ext-gen69"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                        else:
                            print('unable to find specified element on web page, HTML needs enabled...?')
                            error = open('pduerror.txt', 'w+')
                            error.write(ip + " couldn't retrieve power data\n")
                            self.failed += 1
                        self.success += 1
                        pass

                    except Exception as e:
                        print('self.failed to find power values - ' + str(e) + '\n')
                        self.failed += 1

                # If PDU is reachable but script self.failed to log in (probably because it didn't wait for page to load)
                except Exception as e:
                    print('self.failed to navigate PDU w/ login - ' + str(e) + '\n')
                    self.failed += 1
                    pass

            except Exception as e:
                path = 0
                # If login isnt required to see homepage then we omit that process and run whats below #
                try:
                    try:
                        # The path to the power values isn't always the same, so we need to hardcode the possible paths here to check which exists #
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 1
                    except:
                        pass
                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 2
                    except:
                        pass
                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 3
                    except:
                        pass
                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 4
                    except:
                        pass
                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 5
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 6
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 7
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 8
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 9
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 10
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 11
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 12
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 13
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen11"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 14
                    except:
                        pass

                    try:
                        WebDriverWait(driver, .1).until(
                            cond.presence_of_element_located((
                                By.XPATH,
                                '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]')))
                        path = 15
                    except:
                        pass

                    if path == 1:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen62"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen62"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen62"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen62"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 2:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen71"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen71"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen71"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen71"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 3:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen114"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen114"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen114"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen114"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 4:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen117"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen117"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen117"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen117"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                    elif path == 5:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen3"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen3"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen3"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen3"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                    elif path == 6:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen84"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen84"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen84"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen84"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 7:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen4"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen4"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen4"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen4"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 8:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen115"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen115"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen115"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen115"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 9:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen60"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen60"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen60"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen60"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 10:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen67"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen67"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen67"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen67"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 11:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen75"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen75"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen75"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen75"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 12:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen36"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen36"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen36"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen36"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 13:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen110"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen110"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen110"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen110"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 14:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen112"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen112"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen112"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen112"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen112"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen112"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    elif path == 15:
                        self.power1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.power2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.power3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen69"]/div[6]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text
                        self.load1 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen69"]/div[4]/table/tbody/tr/td[2]/div/div/table/tbody/tr/td[2]').text
                        self.load2 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen69"]/div[4]/table/tbody/tr/td[3]/div/div/table/tbody/tr/td[2]').text
                        self.load3 = driver.find_element_by_xpath(
                            '//*[@id="ext-gen69"]/div[4]/table/tbody/tr/td[4]/div/div/table/tbody/tr/td[2]').text

                    else:
                        print('unable to find specified element on web page, HTML needs enabled...?')
                        error = open('pduerror.txt', 'w+')
                        error.write(ip + " couldn't retrieve power data")
                        self.failed += 1
                    # power_dict = {}
                except Exception as e:
                    # power_dict[url] = phase_dict
                    print('self.failed to find power values - ' + str(e) + '\n')
        elif ip not in self.good_list:
            self.unreachable += 1
            pass

        if self.power1 == "":
            self.power1 = '-1'
        if self.load1 == "":
            self.load1 = '-1'
        if self.power2 == '':
            self.power2 = '-1'
        if self.load2 == '':
            self.load2 = '-1'
        if self.power3 == '':
            self.power3 = '-1'
        if self.load3 == '':
            self.load3 = '-1'

        self.phase_dict = {'Phase 1': {'ActivePower': self.power1, 'Load%': self.load1},
                           'Phase 2': {'ActivePower': self.power2, 'Load%': self.load2},
                           'Phase 3': {'ActivePower': self.power3, 'Load%': self.load3}}
        self.power_dict[url] = self.phase_dict

        # print(self.power_dict)
        driver.quit()
        return self.power_dict

    def update_SQL(self, timestamp):
        try:
            conn = pymysql.connect(host='10.20.0.220', port=3306, user='Jackson', password='RockyMountain99',
                                   db='Tracker')
            cursor = conn.cursor()

        except Exception as e:
            print("Database not connectable... No data saved. " + str(e))
            return
        for item in self.power_dict:

            executeStr = """INSERT INTO `Eaton_history` (date_time, ip, P1Power, P1Load,
               P2Power, P2Load, P3Power, P3Load) VALUES (%s,%s,%s,%s,%s,%s,%s,%s) """
            try:
                cursor.execute(executeStr, (
                    timestamp, item, int(self.power_dict[item]['Phase 1']['ActivePower']),
                    int(self.power_dict[item]['Phase 1']['Load%']),
                    int(self.power_dict[item]['Phase 2']['ActivePower']),
                    int(self.power_dict[item]['Phase 2']['Load%']),
                    int(self.power_dict[item]['Phase 3']['ActivePower']),
                    int(self.power_dict[item]['Phase 3']['Load%'])))
            except Exception as e:
                print("issue saving " + item + " " + str(e))
        conn.commit()
        cursor.close()
        conn.close()

    def job(self):
        currentTime = datetime.now()
        timestamp = currentTime.strftime("%Y-%m-%d %H:%M:%S")

        with ThreadPool(20) as pool:
            pool.map(self.getInfo, ipList, chunksize=1)

        print("Data gathered from " + str(self.success) + " PDU's")
        print(str(self.failed) + " PDU's failed to produce a web page...")
        print(str(self.unreachable) + " PDU's were not reachable...")
        # print('Time elapsed: %s minutes' % str(int((t.time() - Z.start_time) / 60)))
        print('Time elapsed: %s minutes' % str(int((t.time() - start) / 60)))
        self.update_SQL(timestamp)
        self.phase_dict.clear()
        self.good_list.clear()
        self.power_dict.clear()


Z = Scraper()

if __name__ == "__main__":
    start = t.time()
    file = open('EatonIPs.txt', 'r')
    ipList = []

    for ip in file:
        ipList.append(ip.strip('\n'))

    # guess a thread pool size which is a tradeoff of number of cpu cores,
    # expected wait time for i/o and memory size.

# Z.job()
schedule.every().hour.at(":00").do(Z.job)

while True:
    schedule.run_pending()
    t.sleep(1)
